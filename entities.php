<?php
/**
  * @author Mike P.J. van Dartel <dartello@gmail.com>
  * @category sql query string manipulation
  * @package SQLBuilder
  */

include_once "attributes.php";
include_once "keyclauses.php";

define("INCLUDE_ATTRIBUTES", true);

/**
 * This class builds up database entities with its attributes
 * 
 * @subpackage Entities
 */
class Entities
{
    /** @constant string REGEX_ALL_FIELDS */
    const REGEX_ALL_FIELDS = "/^((\*|[Aa](LL|ll)))$/";

    /** @var bool $attributes */
    private $attributes;

    /** @var string $tables */
    private $tables;

    /** @var string $fields */
    private $fields;

    /** @var int[] $errors */
	private $errors;

    /**
     * @method __construct()
     * @param string[] $entities
     * @param bool $attributes
     */
    public function __construct(array $entities, bool $attributes = false)
    {
        $this->attributes = $attributes;
        $this->tables = "";
        $this->fields = "";
        $this->errors = array();

        if(!count($entities)) return;

        /** @var string[] $tables_array */
        $tables_array = array();

        /** @var string[] $fields_array */
        $fields_array = array();

        /** @var string $entity */
        /** @var string[]|string $attributes */
        foreach($entities as $entity => $attributes_list) {
            if(is_numeric($entity)) continue;

            /** @var string $table */
            $table = $entity;
            enclose_field($table, "`");
            $tables_array[] = $table;

            if($this->attributes === INCLUDE_ATTRIBUTES && class_exists('Attributes')) {
                if(is_string($attributes_list) && preg_match(self::REGEX_ALL_FIELDS, $attributes_list)) {
                    /** @var Attributes $temp_attributes */
                    $temp_attributes = new Attributes("*");
                    $fields_array[] = $temp_attributes->getAttributes(count($entities) > 1 ? $table : null);
                } elseif(is_array($attributes_list) && count($attributes_list)) {
                    /** @var Attributes $temp_attributes */
                    $temp_attributes = new Attributes($attributes_list);
                    $fields_array[] = $temp_attributes->GetAttributes($table);
                } else {
                    $this->errors[] = 1;
                    break;
                }
            }
        }

        if(count($this->errors)) return;

        $this->tables = count($tables_array) == 1 ? $tables_array[0] : enclose(implode(",\n", $tables_array), "\n");

        if($this->attributes === INCLUDE_ATTRIBUTES && !empty($fields_array)) {
            $this->fields = is_array($fields_array) ? enclose(implode(",\n", $fields_array), "\n") : $fields_array;
        }
    }

    /**
     * @method __destruct()
     * @param string[] $entities
     * @param bool $attributes
     */
    public function __destruct()
    {

    }

    /**
     * @method getTables()
     * @return string
     */
    public function getTables(): string
    {
        if(count($this->errors)) return null;

        return $this->tables;
    }

    /**
     * @method getFields()
     * @return string
     */
    public function getFields(): string
    {
        if(count($this->errors)) return null;

        return $this->fields;
    }

    /**
     * @method create()
     * @param string $tablename
     * @param Attributes $attributes
     * @param PrimaryKey $pk
     * @param ForeignKey $fk
     * @param UniqueKey $uk
     * @return string
     */
    public static function Create(string $tablename, Attributes $attributes, PrimaryKey $pk, ForeignKey $fk = null, UniqueKey $uk = null): string
    {
        enclose_field($name, "`");

        $query = "CREATE TABLE $tablename (\n";
        $query .= ")";

        return $query;
    }

    /**
     * @method Delete()
     * @param string $tablename
     * @return string
     */
    public static function Delete(string $tablename): string
    {
        return "DROP $tablename";
    }

    /**
     * @method Clear()
     * @param string $tablename
     * @return string
     */
    public static function Clear(string $tablename): string
    {
        return "DELETE $tablename";
    }
}

/*
//$data = array("books" => "ALL");
$data = array(
    "books" => array("id", "isbn", "price"),
    "tasks" => array("id", "time", "label"),
);

$entities = new Entities($data, INCLUDE_ATTRIBUTES);
var_dump($entities->getTables().$entities->getFields());
*/
?>