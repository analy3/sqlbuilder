<?php
/**
  * @author Mike P.J. van Dartel <dartello@gmail.com>
  * @category sql query string manipulation
  * @package SQLBuilder
  */

/**
 * Stores a filter term
 * 
 * @subpackage FilterTerm
 */
class FilterTerm
{
    /** @var string $terms */
    private $term_operator;

    /** @var string $key */
    private $key;

    /** @var string $kv_operator */
    private $kv_operator;

    /** @var string $value */
    private $value;

    /**
     * @method __construct()
     * @param Terms (optional) $filters
     */
    public function __construct(string $term_operator, string $key, string $kv_operator, string $value)
    {
        $this->term_operator = uppercase($term_operator);
        $this->key = $key;
        $this->kv_operator = $kv_operator;
        $this->value = $value;
    }

    /**
     * @method getTerms()
     * @param int $index
     * @return string
     */
    public function getTerms(int $index): string
    {
        $str = "";

// Test every member here with private functions and concatenate them to $str

        return $this->$term_operator." ".$this->key." ".$this->kv_operator." ".$this->value;
        //return $str;
    }
}

/**
 * This class enlists an array of Terms
 * 
 * @subpackage FilterTerms
 */
class FilterTerms
{
    /** @var Term[] $terms */
    private $terms;

    /**
     * @method __construct()
     * @param FilterTerm $term
     */
    public function __construct(FilterTerm $term)
    {
        self::AddTerm($term);
    }

    /**
     * @method AddTerm()
     * @param FilterTerm $term
     */
    public function AddTerm(FilterTerm $term)
    {
        $this->terms[] = $term;
    }

    /**
     * @method getTerms()
     * @return string
     */
    public function getTerms(): string
    {
        if(!count($this->terms)) return "";

        /** @var int $index */
        /** @var Term $term */
        foreach($this->terms as $index => $term) {
            $termslist[] = $term->getTerm($index);
        }

        return implode(" ", $termslist);
    }
}

/**
 * This class builds up filter string
 * 
 * @subpackage Filters
 */
class Filters
{
    /** @var Terms $filters */
    private $filters;

    /**
     * @method __construct()
     * @param FilterTerms (optional) $filters
     */
    public function __construct(FilterTerms $filters = null)
    {
        $this->filters = $filters;
    }

    /**
     * @method AddFilter()
     * @param FilterTerm $term
     */
    public function AddFilter(FilterTerm $term)
    {
        $this->filters->AddTerm($term);
    }

    /**
     * @method getFilters()
     * @return string
     */
    public function getFilters(): string
    {
        if(empty($this->filters)) return "1";

        return $this->filters->getTerms();
    }
}
?>