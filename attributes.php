<?php
/**
  * @author Mike P.J. van Dartel <dartello@gmail.com>
  * @category sql query string manipulation
  * @package SQLBuilder
  */

function enclose(string $subject, string $enclosed_char): string
{
    return $enclosed_char.$subject.$enclosed_char;
}

function is_enclosed(string $subject, string $enclosed_char): bool
{
    $lastchar = strlen($subject) - 1;
    return $subject[0] === $enclosed_char && $subject[$lastchar] === $enclosed_char;
}

function enclose_field(&$field, $key, $entity)
{
    if(!is_enclosed($field, "`") && strstr($field, ".") === false) {
        $field = (strlen($entity) ? $entity."." : "").enclose($field, "`");
    }
}
    
/**
 * This class builds up entity attributes
 * 
 * @subpackage Attributes
 */
class Attributes
{
    /** @var string[]|string $attributes */
    private $attributes;

    /**
     * @method __construct()
     * @param string[]|string $attributes
     */
    public function __construct($attributes)
    {
        $this->attributes = $attributes;
    }

    /**
     * @method AddAttribute()
     * @param string $attribute can also be a formula
     */
    public function AddAttribute(string $attribute)
    {
        $this->attributes[] = $attribute;
    }

    /**
     * @method getAttribute()
     * @param string (optional) $table
     * @return string
     */
    public function getAttributes(string $table = null): string
    {
        if($this->attributes === "*") {
            return (empty($table) ? "" : $table.".").$this->attributes;
        }

        /** @var string[] $attributes */
        $attributes = $this->attributes;
        array_walk($attributes, "enclose_field", $table);

        return implode(",\n", $attributes);
    }
}

//$attributes = new Attributes("*");
//var_dump($attributes->getAttributes("`books`"));
?>