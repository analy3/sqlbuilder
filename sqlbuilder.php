<?php
/**
  * @author Mike P.J. van Dartel <dartello@gmail.com>
  * @category sql query string manipulation
  * @package SQLBuilder
  *
  * To do (Method and/or Class) = done:
  * SELECT (SelectEntities)
  *   - WHERE (Filters)
  *   - ORDER BY (SortEntity)
  *   - UNION
  *   - JOIN
  * CREATE TABLE (Entities::Create)
  * DROP TABLE (Entities::Delete)
  * INSERT INTO VALUES
  * DELETE (Enitities::Clear)
  * UPDATE
  *
  * Extra:
  * - set database (setDatabase)
  * - enlist tables (getEntities)
  * - enlist table columns (getColumns)
  */

include_once "entities.php";
include_once "filters.php";
include_once "sortentity.php";


/**
 * This class builds up the SQL query
 * 
 * @method __construct()
 * @method __destruct()
 * @method void ClearQuery()
 * @method string getQuery()
 * @method bool setDatabase(string database)
 * @method bool getEntities()
 * @method bool getAttributes()
 * @method bool SelectEntities(Entities $entities)
 * @method bool AddFilter(Filters $filters)
 */

class SQLBuilder
{
    /** @var string $query */
    private $query;

    /**
     * @method __construct()
     */
    public function __construct()
    {
        self::ClearQuery();
    }

    /**
     * @method __destruct()
     */
    public function __destruct()
    {

    }

    /**
     * @method ClearQuery()
     */
    public function ClearQuery()
    {
        $this->query = "";
    }

    /**
     * @method getQuery()
     * @return string
     */
    public function getQuery(): string
    {
        return $this->query.(empty($this->query) || preg_match("/;\n?$/", $this->query) ? "" : ";\n");
    }

    /**
     * @method setDatabase()
     * @param string $database
     * @return bool
     */
    public function setDatabase(string $database): bool
    {
        enclose_field($database, "`");
        $this->query .= "USE $database;\n";
        return true;
    }

    /**
     * @method getEntities()
     * @return bool
     */
    public function getEntities(): bool
    {
        $this->query .= "SHOW TABLES;\n";
        return true;
    }

    /**
     * @method getAttributes()
     * @param string $table
     * @param string $database
     * @param string (optional) $label_column_name
     * @return bool
     */
    public function getColumns(string $table, string $database, string $label_column_name = "column_name"): bool
    {
        if(empty($table) || empty($database)) return false;

        enclose_field($database, "'");
        enclose_field($table, "'");

        $this->query .= "SELECT\ncolumn_name AS '$label_column_name'\n FROM information_schema.columns\n WHERE table_name = $table\n AND table_schema = $database;\n";
        return true;
    }

    /**
     * @method SelectEntities()
     * @param Entities $entities
     * @return bool
     */
    public function SelectEntities(Entities $entities, Filters $filters = null, SortEntity $sort = null): bool
    {
        /** @var string $tables */
        if(empty($tables = $entities->getTables())) return false;
        /** @var string $fields */
        if(empty($fields = $entities->getFields())) return false;

        $this->query = "SELECT $fields FROM $tables";

        if(!is_null($filters)) {
            $this->query .= "WHERE ".$filters->getFilters()."\n";
        }

        if(!is_null($filters)) {
            $this->query .= "ORDER BY ".$sort->getSort()."\n";
        }

        return true;
    }

    /**
     * @method AddFilter()
     * @param Filters $filters
     * @param FilterTerm $term
     * @return bool
     */
    public function AddFilter(Filters &$filters, FilterTerm $term)
    {
        $filters->AddFilter($term);
    }
}

/* Test cases */
$query = new SQLBuilder();

$query->setDatabase("data");
echo $query->getQuery();
$query->ClearQuery();

$query->getEntities();
echo $query->getQuery();
$query->ClearQuery();

$data = array("books" => "ALL");
$entities = new Entities($data, INCLUDE_ATTRIBUTES);
$query->SelectEntities($entities);
echo $query->getQuery();
//$query->ClearQuery();

$data = array(
    "books" => array("id", "isbn", "price"),
    "tasks" => array("id", "time", "label"),
);
$query->SelectEntities(new Entities($data, INCLUDE_ATTRIBUTES));
$filters = new Filters();
$query->AddFilter($filters);
echo $query->getQuery();
$query->ClearQuery();

//echo "\n".Entities::Create("books", $attributes, $pk, $fk, $uk);
echo "\n".Entities::Clear("books");
echo "\n".Entities::Delete("books");
?>